<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'CheckUserStatus:admin, viewer, new, auth'], function () {
	Route::any('catalog', 'MyCatalogController@ShowCatalog')->name('catalog');
});




Route::group(['middleware' => 'CheckUserStatus:admin'], function () {

	Route::group(['prefix' => 'catalog'], function () {
		Route::get('add',  'MyCatalogController@AddItem')->name('Additem');
		Route::post('add',  'MyCatalogController@AddItemSaving')->name('AddItemSaving');
		
		Route::get('edit',  'MyCatalogController@EditItem')->name('edititem');
		Route::post('edit', 'MyCatalogController@EditItemSaving')->name('edititemSaving');

		Route::get('delete', 'MyCatalogController@DeleteItem')->name('deleteitem');
	});



	Route::group(['prefix' => 'admin'], function () {

 		Route::get('users', 'MyUserController@ShowUsers')->name('userslist');
 		Route::get('users/edit', 'MyUserController@EditUser')->name('edituser');
   		Route::post('users/edit', 'MyUserController@EditUserSaving')->name('EditUserSaving');

   		Route::get('users/delete', 'MyUserController@DeleteUser')->name('deleteuser');
	});
});

