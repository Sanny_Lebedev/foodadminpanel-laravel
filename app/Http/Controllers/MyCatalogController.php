<?php

namespace App\Http\Controllers;
use DateTime;
use Illuminate\Http\Request;
use App\User;
use App\Models\FoodsFT;
use App\Models\FoodsOption;
use Illuminate\Support\Facades\Input;
use DB;

class MyCatalogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */





    public function index()
    {
        return view('home');
    }

    public function ShowCatalog(Request $request)
    {
        $SearchString = $request->input('searchstring');
        $Catname = $request->input('category');
        
    if (!isset($Catname)) $Catname = 'ANY';
    $Category = FoodsFT::where('status','>', 0)
                       ->orderBy('data->category', 'asc')
                       ->distinct('data->category')
                       ->get(['data->category']);

    //var_dump($Category);


     if (strlen($SearchString)>0) {

       if ($Catname != 'ANY') 
         {
        $foods = FoodsFT::where('status','>', 0)
                    ->where('data->name','like','%'.$SearchString.'%')
                    ->where('data->category','like','%'.$Catname.'%')
                    ->orderBy('id', 'desc')->paginate(20);

         } else

        $foods = FoodsFT::where('status','>', 0)
                    ->where('data->name','like','%'.$SearchString.'%')
                    ->orwhere('data->category','like','%'.$SearchString.'%')
                    ->orderBy('id', 'desc')->paginate(20);
   


     } else {
       
        if ($Catname != 'ANY') 
         {
        $foods = FoodsFT::where('status','>', 0)
                        ->where('data->category','like','%'.$Catname.'%')
                        ->orderBy('id', 'desc')->paginate(20);
        }
        else
        $foods = FoodsFT::where('status','>', 0)
                        ->orderBy('id', 'desc')->paginate(20);
     }


        return view('catalog', ['Names' => $foods, 'SearchString' => $SearchString,'Catname'=>$Catname,'Category'=>$Category]);  
    }






    public function EditItem(Request $request)
    {
        $Category = FoodsFT::where('status','>', 0)
                            ->orderBy('data->category', 'asc')
                            ->distinct('data->category')
                            ->get(['data->category']);

         $item = FoodsFT::find($request->input('id'));
        return view('catalog.edititem', ['item' => $item, 'Category'=>$Category]);  
    }


    public function AddItem(Request $request)
    {

        $item                   = (object)[];
        $item->data             = (object)[];
        $item->data->measurment = (object)[];

        $Category = FoodsFT::where('status','>', 0)
                           ->orderBy('data->category', 'asc')
                           ->distinct('data->category')
                           ->get(['data->category']);

        $mesurment = array("porsjon","stort_Glass","handfull","helflaske","stor_Boks","shot","glass","skive","bukett","bunt","ml","kopp","kule","tykk_Skive","boks","liten_Bunt","stang","teskje","liten_Skive","nett","halvflaske","pose","pakke","drape","kurv","liter","stk","tube","kvist","stor_Skive","blad","stk_Stor","bat","plate","stk_Lite","ring","beger","liten_Kopp","stor_Kopp","stor_Pakke","knivsodd","lite_Glass","cl","slice","stk_Filet","bit","barneskje","spiseskje","liten_Pakke","tynn_Skive","fedd","dl","liten_Boks");
        $otherdata = array("name","category","alkohal_Gram","kopper_MilliGram","vitaminE_AlphaTE","cisFlerumettede_Gram","cisEnumettede_Gram","sink_MilliGram","protein_Gram","fosfor_MilliGram","natrium_MilliGram","vitaminB12_MicroGram","vitaminD_MicroGram","vitaminC_MilliGram","magnisium_MilliGram","kcalPer100Gram","kolestral_Gram","jern_MilliGram","fett_Gram","kostfiber_Gram","karbohyrdrate_Gram","kalium_MilliGram","mettedeFettsyrer_Gram","vitaminB6_MilliGram","sukker_Gram","kalsium_MilliGram");
        
        foreach ($mesurment as $key => $value) { $item->data->measurment->$value = 0;   }
        foreach ($otherdata as $key => $value) { $item->data->$value             = 0;   }

        $item->id               = 0;
        $item->data->name       = 'newitem';
        $item->data->category   = 'ANY';
   
        return view('catalog.edititem', ['item' => $item, 'Category'=>$Category]);  
    }


    public function AddItemSaving(Request $request)
    {
    $mesurment = array("porsjon","stort_Glass","handfull","helflaske","stor_Boks","shot","glass","skive","bukett","bunt","ml","kopp","kule","tykk_Skive","boks","liten_Bunt","stang","teskje","liten_Skive","nett","halvflaske","pose","pakke","drape","kurv","liter","stk","tube","kvist","stor_Skive","blad","stk_Stor","bat","plate","stk_Lite","ring","beger","liten_Kopp","stor_Kopp","stor_Pakke","knivsodd","lite_Glass","cl","slice","stk_Filet","bit","barneskje","spiseskje","liten_Pakke","tynn_Skive","fedd","dl","liten_Boks");
    $otherdata = array("name","category","alkohal_Gram","kopper_MilliGram","vitaminE_AlphaTE","cisFlerumettede_Gram","cisEnumettede_Gram","sink_MilliGram","protein_Gram","fosfor_MilliGram","natrium_MilliGram","vitaminB12_MicroGram","vitaminD_MicroGram","vitaminC_MilliGram","magnisium_MilliGram","kcalPer100Gram","kolestral_Gram","jern_MilliGram","fett_Gram","kostfiber_Gram","karbohyrdrate_Gram","kalium_MilliGram","mettedeFettsyrer_Gram","vitaminB6_MilliGram","sukker_Gram","kalsium_MilliGram");
    $input = Input::except('_token');
    

    $options                = (object)[];
    $options->measurment    = (object)[];

    $item = new FoodsFT;
        foreach($input as $name => $value) {
            if (($name == 'name')&&($value=='')) {$value='name';}
            if (($name == 'category')&&($value=='')) {$value='category';}
            if (!isset($value)) {$value=0;}
            if (in_array($name, $mesurment)) { $options->measurment->{$name} = $value;  }
            if (in_array($name, $otherdata)) { $options->{$name} = $value;              }
        } //  foreach($input as $name => $value) {


        $item->data = $options;
        $item->status = 3;
        $date = new DateTime();
        $item->date = $date;
        $item->save();
        
        // changing etag in options table
       
        $options_etag = FoodsOption::where('dataname','etag')->first();
        $options_etag->date = $date;
        $options_etag->data = md5($date->format('Y-m-d H:i:s'));
        $options_etag->save();
        

        return redirect()->route('catalog')->with('status', 'mainpart.Items_Saving_Success');
         
    }

      public function EditItemSaving(Request $request)
    {
                
    $mesurment = array("porsjon","stort_Glass","handfull","helflaske","stor_Boks","shot","glass","skive","bukett","bunt","ml","kopp","kule","tykk_Skive","boks","liten_Bunt","stang","teskje","liten_Skive","nett","halvflaske","pose","pakke","drape","kurv","liter","stk","tube","kvist","stor_Skive","blad","stk_Stor","bat","plate","stk_Lite","ring","beger","liten_Kopp","stor_Kopp","stor_Pakke","knivsodd","lite_Glass","cl","slice","stk_Filet","bit","barneskje","spiseskje","liten_Pakke","tynn_Skive","fedd","dl","liten_Boks");
    $otherdata = array("name","category","alkohal_Gram","kopper_MilliGram","vitaminE_AlphaTE","cisFlerumettede_Gram","cisEnumettede_Gram","sink_MilliGram","protein_Gram","fosfor_MilliGram","natrium_MilliGram","vitaminB12_MicroGram","vitaminD_MicroGram","vitaminC_MilliGram","magnisium_MilliGram","kcalPer100Gram","kolestral_Gram","jern_MilliGram","fett_Gram","kostfiber_Gram","karbohyrdrate_Gram","kalium_MilliGram","mettedeFettsyrer_Gram","vitaminB6_MilliGram","sukker_Gram","kalsium_MilliGram");
    $input = Input::except('_token');

        if ($request->input('id') == $request->input('itemid')) {

        $item = FoodsFT::find($request->input('id'));      
        $options = $item->data;


        foreach($input as $name => $value) {
            if (($name == 'name')&&($value=='')) {$value='name';}
            if (($name == 'category')&&($value=='')) {
                $value=$request->input('categoryone');
            }
            if (!isset($value)) {$value=0;}
            if (in_array($name, $mesurment)) { $options->measurment->{$name} = (float)$value;  }
            if (in_array($name, $otherdata)) { 
                if (($name == 'name')||($name =='category')) {$options->{$name} = $value;             }
                                                        else {$options->{$name} = (float)$value;       }
                 }
        } //  foreach($input as $name => $value) {
       


        $item->data = $options;
        $item->status = 2;
        $date = new DateTime();
        $item->date = $date;
        $item->save();
       
       // changing etag in options table
        $options_etag = FoodsOption::where('dataname','etag')->first();
        $options_etag->data = md5($date->format('Y-m-d H:i:s'));
        $options_etag->date = $date;
        $options_etag->save();
        
        return redirect()->route('catalog')->with('status', 'mainpart.User_Updating_Success');
        } // if ($request->input('id') == $request->input('itemid')) {



    }



    public function DeleteItem(Request $request)
    {
        $item = FoodsFT::find($request->input('id'));
        $item -> delete();
        return redirect()->route('catalog')->with('status', 'mainpart.Items_Delete_Success');
    }

}