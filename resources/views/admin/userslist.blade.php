@extends('admin.maincontainer')
@section('maincontenttab')

@if (session('status'))
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
        @lang(session('status'))
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    @lang(session('error'))
    </div>
@endif


 <div class="table-responsive">
<table class="table table-hover">
  <tr>
    <th>{{ Lang::get('mainpart.User_Name') }}</th>
    <th>{{ Lang::get('mainpart.User_Status') }}</th>
    <th>{{ Lang::get('mainpart.User_Email') }}</th>
    <th>@lang('mainpart.User_Action')</th>
  </tr>
@foreach ($users as $user)
    <tr>
    <td> {{ $user->name }}</td>
    <td> {{ $user->status }}</td>
    <td> {{ $user->email }}</td>
    <td>
        <a href="{{Request::url()}}/edit?id={{$user->id}}"><div class="glyphicon glyphicon-pencil" id="{{$user->id}}" aria-hidden="true"></div></a>
        <a href="{{Request::url()}}/delete?id={{$user->id}}" class="delbtn"><div class="glyphicon glyphicon-trash" aria-hidden="true"></div></a>
    </td>
    </tr>
@endforeach

</table>
</div>




@endsection