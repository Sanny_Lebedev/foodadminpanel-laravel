@extends('admin.maincontainer')
@section('maincontenttab')



      <form  method="post" class="form-horizontal">
        {{ csrf_field() }}
        <input type="hidden" class="form-control" id="userid" name="userid" placeholder="" value="{{ $user->id }}">

        <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">{{ Lang::get('mainpart.User_edit_modaltitle') }}</h4>
        </div>
        <div class="modal-body">
        
        <div class="form-group">
        <label  class="col-sm-2 " for="NameOfUser">{{ Lang::get('mainpart.User_Name') }}</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" id="NameOfUser" name="NameOfUser" placeholder="" value="{{ $user->name }}">
        </div>
        </div>

        <div class="form-group">
        <label class="col-sm-2 " for="EmailOfUser">{{ Lang::get('mainpart.User_Email') }}</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" id="EmailOfUser" name="EmailOfUser" placeholder="" value="{{ $user->email }}">
        </div>
        </div>

    <div class="form-group">
    <label for="StatusOfUser" class="col-sm-2 ">{{ Lang::get('mainpart.User_Status') }}</label>
    <div class="col-sm-10">
    <select class="form-control" id="StatusOfUser" name="StatusOfUser" style="width: 150px;>">
    @if ($user->status == 'new')
    <option  value="new" selected>New</option>
    @else
    <option  value="new">New</option>
    @endif

    @if ($user->status == 'admin')
    <option value="admin" selected>Admin</option>
    @else
    <option value="admin">Admin</option>
    @endif

    @if ($user->status == 'viewer')
    <option value="viewer" selected>Viewer</option>
    @else
    <option value="viewer">Viewer</option>
    @endif
    
    </select>
    </div>
    </div>

        </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{ Lang::get('mainpart.Button_save') }}</button>
        </div>
      
      </form>




@endsection

