@extends('admin.maincontainer')
@section('maincontenttab')



@if (session('status'))
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
        @lang(session('status'))
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    @lang(session('error'))
    </div>
@endif

<form  method="post" class="form-horizontal">
 {{ csrf_field() }}
  <div class="form-group">
 
 <div class="col-sm-10">
 <label for="category">@lang('mainpart.Caption_Category')</label>
    <select class="form-control" id="category" name="category">
        <option  value="ANY" selected>ANY</option>
        @foreach ($Category as $cat)
        @if ($Catname == $cat['?column?'] )
        <option  value="{{ $cat['?column?'] }}" selected>{{ $cat['?column?'] }}</option>
        @else
        <option  value="{{ $cat['?column?'] }}" >{{ $cat['?column?'] }}</option>
        @endif

        @endforeach

    
    </select>
    </div>
    </div>

    <div class="form-group">
    <div class="col-sm-10">

    <label for="category">@lang('mainpart.Caption_SearchString')</label>

      @if (strlen($SearchString)>1)
      <input type="text" class="form-control" name="searchstring" id="searchstring" placeholder="" value="{{$SearchString}}">
      @else
      <input type="text" class="form-control" name="searchstring" id="searchstring" placeholder="">
      @endif
    </div>
    </div>
      <div class="form-group">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary pull-right">@lang('mainpart.Button_Search')</button>
    </div>
    </div>
  </div>

</form>



<p>
<a href="{{Request::url()}}/add" type="submit" class="btn btn-primary">{{ Lang::get('mainpart.Button_ADD_ITEM') }}</a>
</p>
<div class="table-responsive">
<table class="table table-hover">
  <tr>
    <th>{{ Lang::get('mainpart.TAB_Name') }}</th>
    <th>{{ Lang::get('mainpart.TAB_Category') }}</th>
    
    <th>@lang('mainpart.User_Action')</th>
  </tr>
@foreach ($Names as $part)
    <tr>
    <td> {{ $part->data->name }}</td>
    <td> {{ $part->data->category }}</td>
    <td>
        <a href="{{Request::url()}}/edit?id={{$part->id}}"><div class="glyphicon glyphicon-pencil" id="{{$part->id}}" aria-hidden="true"></div></a>
        <a href="{{Request::url()}}/delete?id={{$part->id}}" onclick="return confirm('Are you sure?')" class="delbtn"><div class="glyphicon glyphicon-trash" aria-hidden="true"></div></a>
    </td>
    </tr>
@endforeach

</table>
</div>
{{ $Names->appends(['searchstring' => Request::get('searchstring'), 'category' => Request::get('category')])->links() }}



@endsection

