@extends('admin.maincontainer')
@section('maincontenttab')



      <form  method="post" class="form-horizontal">
        {{ csrf_field() }}
        <input type="hidden" class="form-control" id="itemid" name="itemid" placeholder="" value="{{ $item->id }}">
<button type="submit" class="btn btn-primary">{{ Lang::get('mainpart.Button_save') }}</button>
        <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">{{ Lang::get('mainpart.Item_edit_modaltitle') }}</h4>
        </div>

        <div class="modal-body">
        
        <div class="form-group">
        <label  class="col-sm-2 " for="name">{{ Lang::get('mainpart.Items_Name') }}</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ $item->data->name }}">
        </div>
        </div>

        <div class="form-group">
        <label class="col-sm-2 " for="categoryone">{{ Lang::get('mainpart.Items_Category') }}</label>
        <div class="col-sm-10">
        <select class="form-control" id="categoryone" name="categoryone">
            
                @foreach ($Category as $cat)
                @if ($item->data->category == $cat['?column?'] )
                <option  value="{{ $cat['?column?'] }}" selected>{{ $cat['?column?'] }}</option>
                @else
                <option  value="{{ $cat['?column?'] }}" >{{ $cat['?column?'] }}</option>
                @endif
                @endforeach
      
        </select>
        
        </div>
        </div>


        <div class="form-group">
        <label class="col-sm-2 " for="category">{{ Lang::get('mainpart.Items_Category_new') }}</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" id="category" name="category" placeholder="" value="">
        </div>
        </div>

          <div class="form-group">
          <div class="col-sm-10">
          </div>
          </div>

<div class="onlynum"> 
        <div class="form-group">
        <label class="col-sm-2 " for="alkohal_Gram">{{ Lang::get('mainpart.alkohal_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="alkohal_Gram" name="alkohal_Gram" placeholder="" value="{{ $item->data->alkohal_Gram }}">
        </div>
        <label class="col-sm-2 text-right" for="kopper_MilliGram">{{ Lang::get('mainpart.kopper_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kopper_MilliGram" name="kopper_MilliGram" placeholder="" value="{{ $item->data->kopper_MilliGram }}">
        </div>
        <label class="col-sm-2 text-right" for="vitaminE_AlphaTE">{{ Lang::get('mainpart.vitaminE_AlphaTE') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="vitaminE_AlphaTE" name="vitaminE_AlphaTE" placeholder="" value="{{ $item->data->vitaminE_AlphaTE }}">
        </div> 
        </div>



        <div class="form-group">
        <label class="col-sm-2 " for="cisFlerumettede_Gram">{{ Lang::get('mainpart.cisFlerumettede_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="cisFlerumettede_Gram" name="cisFlerumettede_Gram" placeholder="" value="{{ $item->data->cisFlerumettede_Gram }}">
        </div>
        <label class="col-sm-2 text-right" for="cisEnumettede_Gram">{{ Lang::get('mainpart.cisEnumettede_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="cisEnumettede_Gram" name="cisEnumettede_Gram" placeholder="" value="{{ $item->data->cisEnumettede_Gram }}">
        </div>
        <label class="col-sm-2 text-right" for="sink_MilliGram">{{ Lang::get('mainpart.sink_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="sink_MilliGram" name="sink_MilliGram" placeholder="" value="{{ $item->data->sink_MilliGram }}">
        </div> 
        </div>



        <div class="form-group">
        <label class="col-sm-2 " for="protein_Gram">{{ Lang::get('mainpart.protein_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="protein_Gram" name="protein_Gram" placeholder="" value="{{ $item->data->protein_Gram }}">
        </div>
        <label class="col-sm-2 text-right" for="fosfor_MilliGram">{{ Lang::get('mainpart.fosfor_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="fosfor_MilliGram" name="fosfor_MilliGram" placeholder="" value="{{ $item->data->fosfor_MilliGram }}">
        </div>
        <label class="col-sm-2 text-right" for="natrium_MilliGram">{{ Lang::get('mainpart.natrium_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="natrium_MilliGram" name="natrium_MilliGram" placeholder="" value="{{ $item->data->natrium_MilliGram }}">
        </div> 
        </div>



        <div class="form-group">
        <label class="col-sm-2 " for="vitaminB12_MicroGram">{{ Lang::get('mainpart.vitaminB12_MicroGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="vitaminB12_MicroGram" name="vitaminB12_MicroGram" placeholder="" value="{{ $item->data->vitaminB12_MicroGram }}">
        </div>
        <label class="col-sm-2 text-right" for="vitaminD_MicroGram">{{ Lang::get('mainpart.vitaminD_MicroGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="vitaminD_MicroGram" name="vitaminD_MicroGram" placeholder="" value="{{ $item->data->vitaminD_MicroGram }}">
        </div>
        <label class="col-sm-2 text-right" for="vitaminC_MilliGram">{{ Lang::get('mainpart.vitaminC_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="vitaminC_MilliGram" name="vitaminC_MilliGram" placeholder="" value="{{ $item->data->vitaminC_MilliGram }}">
        </div> 
        </div>



        <div class="form-group">
        <label class="col-sm-2 " for="magnisium_MilliGram">{{ Lang::get('mainpart.magnisium_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="magnisium_MilliGram" name="magnisium_MilliGram" placeholder="" value="{{ $item->data->magnisium_MilliGram }}">
        </div>
        <label class="col-sm-2 text-right" for="kcalPer100Gram">{{ Lang::get('mainpart.kcalPer100Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kcalPer100Gram" name="kcalPer100Gram" placeholder="" value="{{ $item->data->kcalPer100Gram }}">
        </div>
        <label class="col-sm-2 text-right" for="kolestral_Gram">{{ Lang::get('mainpart.kolestral_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kolestral_Gram" name="kolestral_Gram" placeholder="" value="{{ $item->data->kolestral_Gram }}">
        </div> 
        </div>


        <div class="form-group">
        <label class="col-sm-2 " for="jern_MilliGram">{{ Lang::get('mainpart.jern_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="jern_MilliGram" name="jern_MilliGram" placeholder="" value="{{ $item->data->jern_MilliGram }}">
        </div>
        <label class="col-sm-2 text-right" for="fett_Gram">{{ Lang::get('mainpart.fett_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="fett_Gram" name="fett_Gram" placeholder="" value="{{ $item->data->fett_Gram }}">
        </div>
        <label class="col-sm-2 text-right" for="kostfiber_Gram">{{ Lang::get('mainpart.kostfiber_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kostfiber_Gram" name="kostfiber_Gram" placeholder="" value="{{ $item->data->kostfiber_Gram }}">
        </div> 
        </div>


        <div class="form-group">
        <label class="col-sm-2 " for="karbohyrdrate_Gram">{{ Lang::get('mainpart.karbohyrdrate_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="karbohyrdrate_Gram" name="karbohyrdrate_Gram" placeholder="" value="{{ $item->data->karbohyrdrate_Gram }}">
        </div>
        <label class="col-sm-2 text-right" for="kalium_MilliGram">{{ Lang::get('mainpart.kalium_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kalium_MilliGram" name="kalium_MilliGram" placeholder="" value="{{ $item->data->kalium_MilliGram }}">
        </div>
        <label class="col-sm-2 text-right" for="mettedeFettsyrer_Gram">{{ Lang::get('mainpart.mettedeFettsyrer_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="mettedeFettsyrer_Gram" name="mettedeFettsyrer_Gram" placeholder="" value="{{ $item->data->mettedeFettsyrer_Gram }}">
        </div> 
        </div>


        <div class="form-group">
        <label class="col-sm-2 " for="vitaminB6_MilliGram">{{ Lang::get('mainpart.vitaminB6_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="vitaminB6_MilliGram" name="vitaminB6_MilliGram" placeholder="" value="{{ $item->data->vitaminB6_MilliGram }}">
        </div>
        <label class="col-sm-2 text-right" for="sukker_Gram">{{ Lang::get('mainpart.sukker_Gram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="sukker_Gram" name="sukker_Gram" placeholder="" value="{{ $item->data->sukker_Gram }}">
        </div>
        <label class="col-sm-2 text-right" for="kalsium_MilliGram">{{ Lang::get('mainpart.kalsium_MilliGram') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kalsium_MilliGram" name="kalsium_MilliGram" placeholder="" value="{{ $item->data->kalsium_MilliGram }}">
        </div> 
        </div>






<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">@lang('mainpart.Measurment')</h3>
  </div>
  <div class="panel-body">
   

        <div class="form-group">
        <label class="col-sm-1 " for="porsjon">{{ Lang::get('mainpart.porsjon') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="porsjon" name="porsjon" placeholder="" value="{{ $item->data->measurment->porsjon }}">
        </div>
        <label class="col-sm-1 text-right" for="stort_Glass">{{ Lang::get('mainpart.stort_Glass') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stort_Glass" name="stort_Glass" placeholder="" value="{{ $item->data->measurment->stort_Glass }}">
        </div>
        <label class="col-sm-1 text-right" for="handfull">{{ Lang::get('mainpart.handfull') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="handfull" name="handfull" placeholder="" value="{{ $item->data->measurment->handfull }}">
        </div> 
        <label class="col-sm-1 text-right" for="helflaske">{{ Lang::get('mainpart.helflaske') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="helflaske" name="helflaske" placeholder="" value="{{ $item->data->measurment->helflaske }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="stor_Boks">{{ Lang::get('mainpart.stor_Boks') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stor_Boks" name="stor_Boks" placeholder="" value="{{ $item->data->measurment->stor_Boks }}">
        </div>
        <label class="col-sm-1 text-right" for="shot">{{ Lang::get('mainpart.shot') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="shot" name="shot" placeholder="" value="{{ $item->data->measurment->shot }}">
        </div>
        <label class="col-sm-1 text-right" for="glass">{{ Lang::get('mainpart.glass') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="glass" name="glass" placeholder="" value="{{ $item->data->measurment->glass }}">
        </div> 
        <label class="col-sm-1 text-right" for="skive">{{ Lang::get('mainpart.skive') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="skive" name="skive" placeholder="" value="{{ $item->data->measurment->skive }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="bukett">{{ Lang::get('mainpart.bukett') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="bukett" name="bukett" placeholder="" value="{{ $item->data->measurment->bukett }}">
        </div>
        <label class="col-sm-1 text-right" for="bunt">{{ Lang::get('mainpart.bunt') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="bunt" name="bunt" placeholder="" value="{{ $item->data->measurment->bunt }}">
        </div>
        <label class="col-sm-1 text-right" for="ml">{{ Lang::get('mainpart.ml') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="ml" name="ml" placeholder="" value="{{ $item->data->measurment->ml }}">
        </div> 
        <label class="col-sm-1 text-right" for="kopp">{{ Lang::get('mainpart.kopp') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kopp" name="kopp" placeholder="" value="{{ $item->data->measurment->kopp }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="kule">{{ Lang::get('mainpart.kule') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kule" name="kule" placeholder="" value="{{ $item->data->measurment->kule }}">
        </div>
        <label class="col-sm-1 text-right" for="tykk_Skive">{{ Lang::get('mainpart.tykk_Skive') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="tykk_Skive" name="tykk_Skive" placeholder="" value="{{ $item->data->measurment->tykk_Skive }}">
        </div>
        <label class="col-sm-1 text-right" for="boks">{{ Lang::get('mainpart.boks') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="boks" name="boks" placeholder="" value="{{ $item->data->measurment->boks }}">
        </div> 
        <label class="col-sm-1 text-right" for="liten_Bunt">{{ Lang::get('mainpart.liten_Bunt') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="liten_Bunt" name="liten_Bunt" placeholder="" value="{{ $item->data->measurment->liten_Bunt }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="stang">{{ Lang::get('mainpart.stang') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stang" name="stang" placeholder="" value="{{ $item->data->measurment->stang }}">
        </div>
        <label class="col-sm-1 text-right" for="teskje">{{ Lang::get('mainpart.teskje') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="teskje" name="teskje" placeholder="" value="{{ $item->data->measurment->teskje }}">
        </div>
        <label class="col-sm-1 text-right" for="liten_Skive">{{ Lang::get('mainpart.liten_Skive') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="liten_Skive" name="liten_Skive" placeholder="" value="{{ $item->data->measurment->liten_Skive }}">
        </div> 
        <label class="col-sm-1 text-right" for="nett">{{ Lang::get('mainpart.nett') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="nett" name="nett" placeholder="" value="{{ $item->data->measurment->nett }}">
        </div>       
        </div>



        <div class="form-group">
        <label class="col-sm-1 " for="halvflaske">{{ Lang::get('mainpart.halvflaske') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="halvflaske" name="halvflaske" placeholder="" value="{{ $item->data->measurment->halvflaske }}">
        </div>
        <label class="col-sm-1 text-right" for="pose">{{ Lang::get('mainpart.pose') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="pose" name="pose" placeholder="" value="{{ $item->data->measurment->pose }}">
        </div>
        <label class="col-sm-1 text-right" for="pakke">{{ Lang::get('mainpart.pakke') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="pakke" name="pakke" placeholder="" value="{{ $item->data->measurment->pakke }}">
        </div> 
        <label class="col-sm-1 text-right" for="drape">{{ Lang::get('mainpart.drape') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="drape" name="drape" placeholder="" value="{{ $item->data->measurment->drape }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="kurv">{{ Lang::get('mainpart.kurv') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kurv" name="kurv" placeholder="" value="{{ $item->data->measurment->kurv }}">
        </div>
        <label class="col-sm-1 text-right" for="liter">{{ Lang::get('mainpart.liter') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="liter" name="liter" placeholder="" value="{{ $item->data->measurment->liter }}">
        </div>
        <label class="col-sm-1 text-right" for="stk">{{ Lang::get('mainpart.stk') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stk" name="stk" placeholder="" value="{{ $item->data->measurment->stk }}">
        </div> 
        <label class="col-sm-1 text-right" for="tube">{{ Lang::get('mainpart.tube') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="tube" name="tube" placeholder="" value="{{ $item->data->measurment->tube }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="kvist">{{ Lang::get('mainpart.kvist') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="kvist" name="kvist" placeholder="" value="{{ $item->data->measurment->kvist }}">
        </div>
        <label class="col-sm-1 text-right" for="stor_Skive">{{ Lang::get('mainpart.stor_Skive') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stor_Skive" name="stor_Skive" placeholder="" value="{{ $item->data->measurment->stor_Skive }}">
        </div>
        <label class="col-sm-1 text-right" for="blad">{{ Lang::get('mainpart.blad') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="blad" name="blad" placeholder="" value="{{ $item->data->measurment->blad }}">
        </div> 
        <label class="col-sm-1 text-right" for="stk_Stor">{{ Lang::get('mainpart.stk_Stor') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stk_Stor" name="stk_Stor" placeholder="" value="{{ $item->data->measurment->stk_Stor }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="bat">{{ Lang::get('mainpart.bat') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="bat" name="bat" placeholder="" value="{{ $item->data->measurment->bat }}">
        </div>
        <label class="col-sm-1 text-right" for="plate">{{ Lang::get('mainpart.plate') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="plate" name="plate" placeholder="" value="{{ $item->data->measurment->plate }}">
        </div>
        <label class="col-sm-1 text-right" for="stk_Lite">{{ Lang::get('mainpart.stk_Lite') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stk_Lite" name="stk_Lite" placeholder="" value="{{ $item->data->measurment->stk_Lite }}">
        </div> 
        <label class="col-sm-1 text-right" for="ring">{{ Lang::get('mainpart.ring') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="ring" name="ring" placeholder="" value="{{ $item->data->measurment->ring }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="beger">{{ Lang::get('mainpart.beger') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="beger" name="beger" placeholder="" value="{{ $item->data->measurment->beger }}">
        </div>
        <label class="col-sm-1 text-right" for="liten_Kopp">{{ Lang::get('mainpart.liten_Kopp') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="liten_Kopp" name="liten_Kopp" placeholder="" value="{{ $item->data->measurment->liten_Kopp }}">
        </div>
        <label class="col-sm-1 text-right" for="stor_Kopp">{{ Lang::get('mainpart.stor_Kopp') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stor_Kopp" name="stor_Kopp" placeholder="" value="{{ $item->data->measurment->stor_Kopp }}">
        </div> 
        <label class="col-sm-1 text-right" for="stor_Pakke">{{ Lang::get('mainpart.stor_Pakke') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stor_Pakke" name="stor_Pakke" placeholder="" value="{{ $item->data->measurment->stor_Pakke }}">
        </div>       
        </div>


        <div class="form-group">
        <label class="col-sm-1 " for="knivsodd">{{ Lang::get('mainpart.knivsodd') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="knivsodd" name="knivsodd" placeholder="" value="{{ $item->data->measurment->knivsodd }}">
        </div>
        <label class="col-sm-1 text-right" for="lite_Glass">{{ Lang::get('mainpart.lite_Glass') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="lite_Glass" name="lite_Glass" placeholder="" value="{{ $item->data->measurment->lite_Glass }}">
        </div>
        <label class="col-sm-1 text-right" for="cl">{{ Lang::get('mainpart.cl') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="cl" name="cl" placeholder="" value="{{ $item->data->measurment->cl }}">
        </div> 
        <label class="col-sm-1 text-right" for="slice">{{ Lang::get('mainpart.slice') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="slice" name="slice" placeholder="" value="{{ $item->data->measurment->slice }}">
        </div>       
        </div>



        <div class="form-group">
        <label class="col-sm-1 " for="stk_Filet">{{ Lang::get('mainpart.stk_Filet') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="stk_Filet" name="stk_Filet" placeholder="" value="{{ $item->data->measurment->stk_Filet }}">
        </div>
        <label class="col-sm-1 text-right" for="bit">{{ Lang::get('mainpart.bit') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="bit" name="bit" placeholder="" value="{{ $item->data->measurment->bit }}">
        </div>
        <label class="col-sm-1 text-right" for="barneskje">{{ Lang::get('mainpart.barneskje') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="barneskje" name="barneskje" placeholder="" value="{{ $item->data->measurment->barneskje }}">
        </div> 
        <label class="col-sm-1 text-right" for="spiseskje">{{ Lang::get('mainpart.spiseskje') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="spiseskje" name="spiseskje" placeholder="" value="{{ $item->data->measurment->spiseskje }}">
        </div>       
        </div>



        <div class="form-group">
        <label class="col-sm-1 " for="liten_Pakke">{{ Lang::get('mainpart.liten_Pakke') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="liten_Pakke" name="liten_Pakke" placeholder="" value="{{ $item->data->measurment->liten_Pakke }}">
        </div>
        <label class="col-sm-1 text-right" for="tynn_Skive">{{ Lang::get('mainpart.tynn_Skive') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="tynn_Skive" name="tynn_Skive" placeholder="" value="{{ $item->data->measurment->tynn_Skive }}">
        </div>
        <label class="col-sm-1 text-right" for="fedd">{{ Lang::get('mainpart.fedd') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="fedd" name="fedd" placeholder="" value="{{ $item->data->measurment->fedd }}">
        </div> 
        <label class="col-sm-1 text-right" for="dl">{{ Lang::get('mainpart.dl') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="dl" name="dl" placeholder="" value="{{ $item->data->measurment->dl }}">
        </div>       
        </div>
</div> 

        <div class="form-group">
        <label class="col-sm-1 " for="liten_Boks">{{ Lang::get('mainpart.liten_Boks') }}</label>
        <div class="col-sm-2">
        <input type="text" class="form-control" id="liten_Boks" name="liten_Boks" placeholder="" value="{{ $item->data->measurment->liten_Boks }}">
        </div>           
        </div>

  </div>
</div>






        </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{ Lang::get('mainpart.Button_save') }}</button>
        </div>
      
      </form>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $('.onlynum input').bind("change keyup input click", function() {
 
  this.value = this.value.replace(/[^\d\.]/g, "");
        if(this.value.match(/\./g))
        if(this.value.match(/\./g).length > 1) {
        this.value = this.value.substr(0, this.value.lastIndexOf("."));
        }
   
      if(this.value)
      if(this.value.length < 2) {
            if (this.value.match(/\./g))
            this.value = this.value.substr(0, this.value.lastIndexOf("."));
        }


});

</script>>

@endsection

