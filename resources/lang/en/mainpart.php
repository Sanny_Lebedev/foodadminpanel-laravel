<?php

return [

    'menu_Main_Catalog'  => 'CATALOG',
    'Caption_Category'   => 'Category',
    'Caption_SearchString'=> 'Search string',

    'Button_close' 	     => 'Close',
    'Button_save' 	     => 'Save changes',
    'Button_Search'      => 'Search',
    'Button_ADD_ITEM'    => 'Add new item',
    'Items_Saving_Success'              => 'Information successfully saved!',
    'Items_Delete_Success'              => 'Item successfully deleted!',

    'User_Group_add_new'   				=> 'Add new group',
    'User_Group_add_new_modaltitle'   	=> 'New group',
    'User_Name'   				        => 'Name',
    'User_Status'   				    => 'Status',
    'User_Email'                        => 'Email',
    'User_Action'          				=> 'Action',

    'User_Group_Double_entry'   => 'Double entry!',
    'User_Group_Short_Name'     => 'Name is too short!',
    'User_Updating_Success'     => 'Information successfully updated!',
    'User_Deleted_Success'      => 'This user was deleted successfully!',
    'User_Deleting_conf'        => 'Are you really wand to delete this user?',
    'User_edit_modaltitle'      => 'Edit user information',
    'Cant_delete_user'          => 'You can not remove a single user',


    'TAB_Category'              => 'Category',
    'TAB_Name'                  => 'Name',

    'Item_edit_modaltitle'      => 'Change in characteristics',

    'Items_Name'                => 'Items name',
    'Items_Category'            => 'Items Category', 
    'Items_Category_new'        => 'or make new',

    'alkohal_Gram'          => 'alkohal (Gram)',
    'kopper_MilliGram'      => 'kopper (MilliGram)',   
    'vitaminE_AlphaTE'      => 'vitaminE AlphaTE',

    'cisFlerumettede_Gram'  => 'cisFlerumettede (Gram)',
    'cisEnumettede_Gram'    => 'cisEnumettede (Gram)',
    'sink_MilliGram'        => 'sink (MilliGram)',

    'protein_Gram'          => 'protein (Gram)',
    'fosfor_MilliGram'      => 'fosfor (MilliGram)',
    'natrium_MilliGram'     => 'natrium (MilliGram)',

    'vitaminB12_MicroGram'  => 'vitamin B12 (MicroGram)',
    'vitaminD_MicroGram'    => 'vitamin D (MicroGram)',
    'vitaminC_MilliGram'    => 'vitamin C (MilliGram)',

    'magnisium_MilliGram'   => 'magnisium (MilliGram)',
    'kcalPer100Gram'        => 'kcal (per 100 Gram)',
    'kolestral_Gram'        => 'kolestral (Gram)',

    'jern_MilliGram'        => 'jern (MilliGram)',
    'fett_Gram'             => 'fett (Gram)',
    'kostfiber_Gram'        => 'kostfiber (Gram)',

    'karbohyrdrate_Gram'    => 'karbohyrdrate (Gram)',
    'kalium_MilliGram'      => 'kalium (MilliGram)',
    'mettedeFettsyrer_Gram' => 'mettedeFettsyrer (Gram)',

    'vitaminB6_MilliGram'   => 'vitamin B6 (MilliGram)',
    'sukker_Gram'           => 'sukker (Gram)',
    'kalsium_MilliGram'     => 'kalsium (MilliGram)',

    'Measurment'            => 'Measurment',
    'porsjon'               => 'porsjon',
    'stort_Glass'           => 'stort Glass',
    'handfull'              => 'handfull',
    'helflaske'             => 'helflaske',

    'stor_Boks'             => 'stor Boks',
    'shot'                  => 'shot',
    'glass'                 => 'glass',
    'skive'                 => 'skive',

    'bukett'                => 'bukett',
    'bunt'                  => 'bunt',
    'ml'                    => 'ml',
    'kopp'                  => 'kopp',

    'kule'                  => 'kule',
    'tykk_Skive'            => 'tykk Skive',
    'boks'                  => 'boks',
    'liten_Bunt'            => 'liten_Bunt',

    'stang'                 => 'stang',
    'teskje'                => 'teskje',
    'liten_Skive'           => 'liten Skive',
    'nett'                  => 'nett',

    'halvflaske'            => 'halvflaske',
    'pose'                  => 'pose',
    'pakke'                 => 'pakke',
    'drape'                 => 'drape',

    'kurv'                  => 'kurv',
    'liter'                 => 'liter',
    'stk'                   => 'stk',
    'tube'                  => 'tube',

    'kvist'                 => 'kvist',
    'stor_Skive'            => 'stor Skive',
    'blad'                  => 'blad',
    'stk_Stor'              => 'stk Stor',

    'bat'                   => 'bat',
    'plate'                 => 'plate',
    'stk_Lite'              => 'stk Lite',
    'ring'                  => 'ring',

    'beger'                 => 'beger',
    'liten_Kopp'            => 'liten Kopp',
    'stor_Kopp'             => 'stor Kopp',
    'stor_Pakke'            => 'stor Pakke',

    'knivsodd'              => 'knivsodd',
    'lite_Glass'            => 'lite Glass',
    'cl'                    => 'cl',
    'slice'                 => 'slice',

    'stk_Filet'             => 'stk Filet',
    'bit'                   => 'bit',
    'barneskje'             => 'barneskje',
    'spiseskje'             => 'spiseskje',

    'liten_Pakke'           => 'litenPakke',
    'tynn_Skive'            => 'tynn Skive',
    'fedd'                  => 'fedd',
    'dl'                    => 'dl',

    'liten_Boks'            => 'liten Boks',






];
