<?php

return [
    'menu_languages_rus' => 'Russian',
    'menu_languages_eng' => 'English',
    'left_menu_settings' => 'Profile settings',
    'menu_top_adminsetting' => 'Settings',
    'menu_top_users' => 'Users list',

];