<?php

namespace FoodPannel\Models;

use Illuminate\Database\Eloquent\Model;

class FoodsFT extends Model
{
        protected $table = 'ft2';
      public $timestamps = false;
       protected $casts = [
         		'data' => 'object',
        ];
       

        protected $fillable = [ 'data', 'date','status'];


       

}
