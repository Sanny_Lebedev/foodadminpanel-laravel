<?php

namespace FoodPannel\Http\Middleware;

use Closure;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle($request, Closure $next, ...$roles)
    {
       
        $user = $request->user();
        if (!($user && $user->hasRole($roles)))
        {
 
            return redirect('home');
        }
       
        
         return $next($request);
    }

}
