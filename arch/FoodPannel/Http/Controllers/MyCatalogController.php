<?php

namespace FoodPannel\Http\Controllers;
use DateTime;
use Illuminate\Http\Request;
use FoodPannel\User;
use FoodPannel\Models\FoodsFT;
use Illuminate\Support\Facades\Input;
use DB;

class MyCatalogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */





    public function index()
    {
        return view('home');
    }

    public function ShowCatalog(Request $request)
    {
        $SearchString = $request->input('searchstring');
    
        
     if (strlen($SearchString)>0) {

        $foods = FoodsFT::where('status','>', 0)
                    ->where('data->name','like','%'.$SearchString.'%')
                    ->orwhere('data->category','like','%'.$SearchString.'%')
                    ->orderBy('id', 'desc')->paginate(20);
                    //$foods->setPath('custom/url');

     } else {
        $foods = FoodsFT::where('status','>', 0)
                        ->orderBy('id', 'desc')->paginate(20);
     }


        return view('catalog', ['Names' => $foods, 'SearchString' => $SearchString]);  
    }


    public function EditItem(Request $request)
    {
         $item = FoodsFT::find($request->input('id'));
        return view('catalog.edititem', ['item' => $item]);  
    }


    public function AddItem(Request $request)
    {

        $item                   = (object)[];
        $item->data             = (object)[];
        $item->data->measurment = (object)[];

        $mesurment = array("porsjon","stort_Glass","handfull","helflaske","stor_Boks","shot","glass","skive","bukett","bunt","ml","kopp","kule","tykk_Skive","boks","liten_Bunt","stang","teskje","liten_Skive","nett","halvflaske","pose","pakke","drape","kurv","liter","stk","tube","kvist","stor_Skive","blad","stk_Stor","bat","plate","stk_Lite","ring","beger","liten_Kopp","stor_Kopp","stor_Pakke","knivsodd","lite_Glass","cl","slice","stk_Filet","bit","barneskje","spiseskje","liten_Pakke","tynn_Skive","fedd","dl","liten_Boks");
        $otherdata = array("name","category","alkohal_Gram","kopper_MilliGram","vitaminE_AlphaTE","cisFlerumettede_Gram","cisEnumettede_Gram","sink_MilliGram","protein_Gram","fosfor_MilliGram","natrium_MilliGram","vitaminB12_MicroGram","vitaminD_MicroGram","vitaminC_MilliGram","magnisium_MilliGram","kcalPer100Gram","kolestral_Gram","jern_MilliGram","fett_Gram","kostfiber_Gram","karbohyrdrate_Gram","kalium_MilliGram","mettedeFettsyrer_Gram","vitaminB6_MilliGram","sukker_Gram","kalsium_MilliGram");
        
        foreach ($mesurment as $key => $value) { $item->data->measurment->$value = 0;   }
        foreach ($otherdata as $key => $value) { $item->data->$value             = 0;   }

        $item->id               = 0;
        $item->data->name       = 'newitem';
        $item->data->category   = 'category';
   
        return view('catalog.edititem', ['item' => $item]);  
    }


    public function AddItemSaving(Request $request)
    {
    $mesurment = array("porsjon","stort_Glass","handfull","helflaske","stor_Boks","shot","glass","skive","bukett","bunt","ml","kopp","kule","tykk_Skive","boks","liten_Bunt","stang","teskje","liten_Skive","nett","halvflaske","pose","pakke","drape","kurv","liter","stk","tube","kvist","stor_Skive","blad","stk_Stor","bat","plate","stk_Lite","ring","beger","liten_Kopp","stor_Kopp","stor_Pakke","knivsodd","lite_Glass","cl","slice","stk_Filet","bit","barneskje","spiseskje","liten_Pakke","tynn_Skive","fedd","dl","liten_Boks");
    $otherdata = array("name","category","alkohal_Gram","kopper_MilliGram","vitaminE_AlphaTE","cisFlerumettede_Gram","cisEnumettede_Gram","sink_MilliGram","protein_Gram","fosfor_MilliGram","natrium_MilliGram","vitaminB12_MicroGram","vitaminD_MicroGram","vitaminC_MilliGram","magnisium_MilliGram","kcalPer100Gram","kolestral_Gram","jern_MilliGram","fett_Gram","kostfiber_Gram","karbohyrdrate_Gram","kalium_MilliGram","mettedeFettsyrer_Gram","vitaminB6_MilliGram","sukker_Gram","kalsium_MilliGram");
    $input = Input::except('_token');
    

    $options                = (object)[];
    $options->measurment    = (object)[];

    $item = new FoodsFT;
        foreach($input as $name => $value) {
            if (($name == 'name')&&($value=='')) {$value='name';}
            if (($name == 'category')&&($value=='')) {$value='category';}
            if (!isset($value)) {$value=0;}
            if (in_array($name, $mesurment)) { $options->measurment->{$name} = $value;  }
            if (in_array($name, $otherdata)) { $options->{$name} = $value;              }
        } //  foreach($input as $name => $value) {


        $item->data = $options;
        $item->status = 3;
        $date = new DateTime();
        $item->date = $date;
        $item->save();
        return redirect()->route('catalog')->with('status', 'mainpart.Items_Saving_Success');
    }

      public function EditItemSaving(Request $request)
    {
                
    $mesurment = array("porsjon","stort_Glass","handfull","helflaske","stor_Boks","shot","glass","skive","bukett","bunt","ml","kopp","kule","tykk_Skive","boks","liten_Bunt","stang","teskje","liten_Skive","nett","halvflaske","pose","pakke","drape","kurv","liter","stk","tube","kvist","stor_Skive","blad","stk_Stor","bat","plate","stk_Lite","ring","beger","liten_Kopp","stor_Kopp","stor_Pakke","knivsodd","lite_Glass","cl","slice","stk_Filet","bit","barneskje","spiseskje","liten_Pakke","tynn_Skive","fedd","dl","liten_Boks");
    $otherdata = array("name","category","alkohal_Gram","kopper_MilliGram","vitaminE_AlphaTE","cisFlerumettede_Gram","cisEnumettede_Gram","sink_MilliGram","protein_Gram","fosfor_MilliGram","natrium_MilliGram","vitaminB12_MicroGram","vitaminD_MicroGram","vitaminC_MilliGram","magnisium_MilliGram","kcalPer100Gram","kolestral_Gram","jern_MilliGram","fett_Gram","kostfiber_Gram","karbohyrdrate_Gram","kalium_MilliGram","mettedeFettsyrer_Gram","vitaminB6_MilliGram","sukker_Gram","kalsium_MilliGram");
    $input = Input::except('_token');

        if ($request->input('id') == $request->input('itemid')) {

        $item = FoodsFT::find($request->input('id'));      
        $options = $item->data;

        foreach($input as $name => $value) {
            if (($name == 'name')&&($value=='')) {$value='name';}
            if (($name == 'category')&&($value=='')) {$value='category';}
            if (!isset($value)) {$value=0;}
            if (in_array($name, $mesurment)) { $options->measurment->{$name} = $value;  }
            if (in_array($name, $otherdata)) { $options->{$name} = $value;              }
        } //  foreach($input as $name => $value) {
       


        $item->data = $options;
        $item->status = 2;
        $date = new DateTime();
        $item->date = $date;
        $item->save();
        return redirect()->route('catalog')->with('status', 'mainpart.User_Updating_Success');
        } // if ($request->input('id') == $request->input('itemid')) {



    }



    public function DeleteItem(Request $request)
    {
        $item = FoodsFT::find($request->input('id'));
        $item -> delete();
        return redirect()->route('catalog')->with('status', 'mainpart.Items_Delete_Success');
    }

}