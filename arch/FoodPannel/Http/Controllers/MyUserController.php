<?php

namespace FoodPannel\Http\Controllers;

use Illuminate\Http\Request;
use FoodPannel\User;

class MyUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */





    public function index()
    {
        return view('home');
    }


    public function ShowUsers()
    {



     $UsersList = User::all();
     return view('admin.userslist', ['users' => $UsersList]);   
    }


     public function EditUser(Request $request)
    {

      $user = User::find($request->input('id'));
      return view('admin.usersedit', ['user' => $user]);  
    }


    public function EditUserSaving(Request $request)
    {
        $NameOfUser     = $request->input('NameOfUser');
        $EmailOfUser    = $request->input('EmailOfUser');
        $userid         = $request->input('userid');
        $StatusOfUser   = $request->input('StatusOfUser');
        
        if (strlen($NameOfUser)<1) return redirect()->route('userslist')->with('error', 'mainpart.User_Group_Short_Name');
           $NewUserRole = User::find($userid);
           $NewUserRole -> name   = $NameOfUser;
           $NewUserRole -> email  = $EmailOfUser;
           $NewUserRole -> status = $StatusOfUser;
           $NewUserRole -> save();
           return redirect()->route('userslist')->with('status', 'mainpart.User_Updating_Success');
        

    }
   
    public function DeleteUser (Request $request)
    {
        
         $users = User::All();
         if (count($users)<2) return redirect()->route('userslist')->with('error', 'mainpart.Cant_delete_user');

        $userrole = User::find($request->input('id'));

        $userrole -> delete();
        return redirect()->route('userslist')->with('status', 'mainpart.User_Deleted_Success');
    }

}
