<?php

namespace FoodPannel;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


        public function hasRole($roles) {
        $Flag    = false;
        $RolesID = $this->status;
        
        if (!is_array($roles)) {
            $array = array($roles, 'Auth');
            $roles = $array;
        }

             
        if (array_search($RolesID, $roles)!==false) { $Flag=true; }      
        
        return $Flag;

        
    }
}
